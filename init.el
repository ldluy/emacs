(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-initialize)

(org-babel-load-file (expand-file-name "~/.config/emacs/config.org"))
